import csv
import matplotlib.pyplot as plt
import pandas as pd

aseanCountries = ['Brunei', 'Cambodia', 'Indonesia', 'Laos', 'Malaysia',
                  'Myanmar', 'Philippines', 'Singapore', 'Thailand', 'Vietnam']
saarcCountries = ['Bangladesh', 'Bhutan', 'India', 'Maldives', 'Nepal',
                  'Pakistan', 'Sri Lanka']

Years = ["2004", '2005', '2006', '2007', '2008', '2009', '2010', '2011',
         '2012', '2013', '2014']
colors = ["red", "cyan", "orange", "green", "black", "blue", "purple",
          "yellow", "violet"]


def rawdata(filename):  # collecting the .csv files and converting into data
    with open(filename, "r") as file:
        csvReader = csv.DictReader(file)
        data = list(csvReader)

    return data


def IndiaPopulationOverYears(populationCsv):
    # getting data from csv file
    countriesData = rawdata(populationCsv)

    indianPopulation = {}
    # getting of indian population from the totla population
    for countries in countriesData:

        if countries["Region"] == "India":
            indianPopulation[countries['Year']] = countries['Population']

    # print(indianPopulation)

    plt.bar(indianPopulation.keys(), indianPopulation.values())

    plt.gcf().autofmt_xdate()
    plt.show()


def ForTheYear2014BarChartOfPopulationOfASEANcountries(populationCsv):
    # getting data from csv file
    populationData = rawdata(populationCsv)

    AseanCountriesPopulation = {}
    # getting population of asean countries in the year 2014
    for country in populationData:

        if country['Region'] in aseanCountries and country['Year'] == "2014":

            AseanCountriesPopulation[country["Region"]] = country["Population"]

    # print(AseanCountriesPopulation)

    plt.bar(AseanCountriesPopulation.keys(), AseanCountriesPopulation.values(),
            align='center', alpha=1)
    plt.gcf().autofmt_xdate()

    plt.xlabel("ASEAN Countries")
    plt.ylabel("Population")
    plt.title(" 2014 Bar Chart of population of ASEAN countries")
    plt.show()


def TotalPopulationOfSAARCcountries(populationCsv):
    # getting data from csv file
    populationData = rawdata(populationCsv)

    saarcCountriesPopulation = {}
    # getting population of SAARC countries over all years and
    # storing it in a dictionary
    for country in populationData:

        if country['Region'] in saarcCountries:

            if country["Year"] in saarcCountriesPopulation:

                saarcCountriesPopulation[country["Year"]] += int(float(country["Population"]))

            else:
                saarcCountriesPopulation[country["Year"]] = int(float(country["Population"]))

    # print(saarcCountriesPopulation)

    plt.bar(saarcCountriesPopulation.keys(), saarcCountriesPopulation.values())
    plt.xlabel("Population of SAARC countries")
    plt.ylabel("Years")
    plt.gcf().autofmt_xdate()
    plt.show()


def ASEANpopulationOverYears(populationCsv):
    populationData = rawdata(populationCsv)

    values = [0]*len(Years)
    # converting years from 2004 to 2014 to dectionary and assigning 0 to them
    # yearsDict = dict(zip(Years, values))

    # print(yearsDict)

    # getting asean countries list and assigning yearsDict to it
    # aseanCountriesPopulation = dict(zip(aseanCountries,
                                        # [yearsDict]*len(aseanCountries)))
    aseanCountriesPopulation = {}

    for countries in aseanCountries :

        yearsDict = {}
        for year in Years:
            yearsDict[year] = 0

        aseanCountriesPopulation[countries] = yearsDict

    # print(aseanCountriesPopulation["Malaysia"]["2006"])
    # getting asean countries population from 2004 to 2014
    for country in populationData:
        if country["Region"] in aseanCountries and country["Year"] in Years:
            year = country["Year"]
            countryName = country["Region"]
            aseanCountriesPopulation[countryName][year] = float(country["Population"])

    print(aseanCountriesPopulation)

    # population of asean countries over 2004 to 2014
    populationOverYears = list(aseanCountriesPopulation.values())
    # print(populationOverYears)

    countriesListDictonary = {}
    count = 0
    # getting population of asean countries into a dictionary and
    # convert them to list of values
    for country in aseanCountries:
        countriesListDictonary[country] = list(populationOverYears[count].values())
        count = count + 1
    # print(countriesListDictonary)

    # converting the values into list
    populationOfaseanCountries = list(countriesListDictonary.values())
    # print(populationOfaseanCountries)

    # adding the name of the country to list to show it in bar chart
    for i in range(len(aseanCountries)):
        populationOfaseanCountries[i] = [aseanCountries[i]] + populationOfaseanCountries[i]

    # print(populationOfaseanCountries)

    # converting data into bar chart
    df = pd.DataFrame(populationOfaseanCountries, columns=["Country", *Years])

    df.plot(x="Country", y=Years, kind="bar", figsize=(10, 10))

    plt.show()


def main():
    growthCsv = "/home/akhil118/Desktop/git/Population-growth/population-estimates.csv"

    IndiaPopulationOverYears(growthCsv)
    ForTheYear2014BarChartOfPopulationOfASEANcountries(growthCsv)
    TotalPopulationOfSAARCcountries(growthCsv)
    ASEANpopulationOverYears(growthCsv)


main()
